from django.urls import path
from . import views

app_name = 'trialerrordifoldersiniaja'

urlpatterns = [
    path('', views.trial, name="trial"),
]