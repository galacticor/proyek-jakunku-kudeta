from django.urls import path
from . import views

app_name = 'aplikasiutama'

urlpatterns = [
    path('', views.landing, name="landing"),
]